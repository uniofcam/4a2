
      subroutine euler_iteration(av,g,ss,ch)

!     This subroutine calculates the fluxes into each cell and then sums them to
!     update the primary flow properties

!     Explicitly declare the required variables
      use types
      use stencils
      implicit none
      type(t_appvars), intent(in) :: av
      type(t_grid), intent(inout) :: g
      logical, intent(in) :: ss
      logical, intent(in) :: ch
      real, dimension(g%ni,g%nj) :: mass_i, h_i, px_i, py_i
      real, dimension(g%ni,g%nj) :: mass_j, h_j, px_j, py_j

      call calc_fluxes(g, mass_i, mass_j, h_i, h_j, px_i, px_j, py_i, py_j, ss)
      call sum_fluxes(av, mass_i, mass_j, g%area, g%ro_start, g%dro) ! improv-1
      if(.not. ch) then
            call sum_fluxes(av, h_i, h_j, g%area, g%roe_start, g%droe) ! improv-1
      end if
      call sum_fluxes(av, px_i, px_j, g%area, g%rovx_start, g%drovx) ! improv-1
      call sum_fluxes(av, py_i, py_j, g%area, g%rovy_start, g%drovy) ! improv-1

      ! improv-1
      g%ro = g%ro_start
      if(.not. ch) then
            g%roe = g%roe_start
      end if
      g%rovx = g%rovx_start
      g%rovy = g%rovy_start

!     Add artificial viscosity by smoothing all of the primary flow variables
      ! improv-3 ! improv-4b
      call smooth_array(av,g%ro,g%corr_ro)
      if(.not. ch) then
            call smooth_array(av,g%roe,g%corr_roe)
      end if
      call smooth_array(av,g%rovx,g%corr_rovx)
      call smooth_array(av,g%rovy,g%corr_rovy)
      

      end subroutine euler_iteration


