
      module stencils

!     Packaging a subroutine in a module allows it to recieve the data
!     conveniently as assumed shape arrays
      
      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine interp_2(y_i, y_j, x1, x2, w1_i, w2_i, w1_j, w2_j, ni, nj)

      implicit none
      real, intent(inout) :: y_i(:,:), y_j(:,:)
      real, intent(in) :: x1(:,:), x2(:,:)
      real, intent(in) :: w1_i(:,:), w2_i(:,:), w1_j(:,:), w2_j(:,:)
      integer, intent(in) :: ni, nj

      y_i = ( ( x1(:,1:nj-1) + x1(:,2:nj) ) * w1_i + &
            ( x2(:,1:nj-1) + x2(:,2:nj) ) * w2_i ) / 2
      y_j = ( ( x1(1:ni-1,:) + x1(2:ni,:) ) * w1_j + &
            ( x2(1:ni-1,:) + x2(2:ni,:) ) * w2_j ) / 2

      end subroutine interp_2

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine interp(y_i, y_j, x1, x2, w1_i, w2_i, w1_j, w2_j, ni, nj)

      implicit none
      real, intent(inout) :: y_i(:,:), y_j(:,:)
      real, intent(in) :: x1(:,:), x2(:,:)
      real, intent(in) :: w1_i(:,:), w2_i(:,:), w1_j(:,:), w2_j(:,:)
      integer, intent(in) :: ni, nj
      real, dimension(4) :: arr
      integer :: i, j
      arr = (/-2., -1., 1., 2./)

      call interp_2(y_i, y_j, x1, x2, w1_i, w2_i, w1_j, w2_j, ni, nj)

      do i=2,ni-2
            do j=2,nj-2
                  y_i(i,j) = ( int_f(4, arr, x1(i,j-1:j+2)) * w1_i(i,j) + &
                              int_f(4, arr, x2(i,j-1:j+2)) * w2_i(i,j) )
                  y_j(i,j) = ( int_f(4, arr, x1(i-1:i+2,j)) * w1_j(i,j) + &
                              int_f(4, arr, x2(i-1:i+2,j)) * w2_j(i,j) )
            end do
      end do
      
      end subroutine interp

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      function int_f(n, x, y)
      implicit none
      real, intent(in) :: x(:), y(:) ! 4
      integer, intent(in) :: n
      real :: int_f
      real :: x1, x2, dx, sum
      integer :: i, d = 10

      x1 = x(1)
      x2 = x(n)
      dx = (x2 - x1) / d
      sum = 0.0
      do i = 1, d
            sum = sum + interp_l(x, y, x1 + (i - 1) * dx) * dx
      end do
      int_f = sum / (x2 - x1)
      end function int_f

      function interp_l(x, y, x0)
      implicit none
      real, intent(in) :: x(:), y(:) ! 4
      real, intent(in) :: x0
      real :: interp_l, l
      integer :: i, j, n 
      n = size(x)
      interp_l = 0.0
      do i = 1, n
      l = 1.0
      do j = 1, n
            if (j /= i) then
                  l = l * (x0 - x(j)) / (x(i) - x(j))
            end if
      end do
      interp_l = interp_l + l * y(i)
      end do
      end function interp_l

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine calc_fluxes(g,mass_i,mass_j,h_i,h_j,px_i,px_j,py_i,py_j,ss)

      use types
      implicit none
      type(t_grid), intent(in) :: g
      real, intent(inout) :: mass_i(:,:), mass_j(:,:), h_i(:,:), h_j(:,:), &
                             px_i(:,:), px_j(:,:), py_i(:,:), py_j(:,:)
      logical, intent(in) :: ss
      real, dimension(g%ni,g%nj) :: zero
      zero(:,:) = 0

      ! open(unit=12,file='debug.txt')

      if(ss) then
            call interp(mass_i, mass_j, g%rovx, g%rovy, g%lx_i, g%ly_i, g%lx_j, g%ly_j, g%ni, g%nj)
            where(g%wall(1:g%ni-1,:) .and. g%wall(2:g%ni,:)) mass_j = 0
            where(g%wall(:,1:g%nj-1) .and. g%wall(:,2:g%nj)) mass_i = 0
            call interp(h_i, h_j, g%hstag, zero, mass_i, mass_i, mass_j, mass_j, g%ni, g%nj)
            call interp(px_i, px_j, g%vx, g%p, mass_i, g%lx_i, mass_j, g%lx_j, g%ni, g%nj)
            call interp(py_i, py_j, g%vy, g%p, mass_i, g%ly_i, mass_j, g%ly_j, g%ni, g%nj)
      else
            call interp_2(mass_i, mass_j, g%rovx, g%rovy, g%lx_i, g%ly_i, g%lx_j, g%ly_j, g%ni, g%nj)
            where(g%wall(1:g%ni-1,:) .and. g%wall(2:g%ni,:)) mass_j = 0
            where(g%wall(:,1:g%nj-1) .and. g%wall(:,2:g%nj)) mass_i = 0
            call interp_2(h_i, h_j, g%hstag, zero, mass_i, mass_i, mass_j, mass_j, g%ni, g%nj)
            call interp_2(px_i, px_j, g%vx, g%p, mass_i, g%lx_i, mass_j, g%lx_j, g%ni, g%nj)
            call interp_2(py_i, py_j, g%vy, g%p, mass_i, g%ly_i, mass_j, g%ly_j, g%ni, g%nj)
      end if

      end subroutine calc_fluxes
     
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine sum_fluxes(av,flux_i,flux_j,area,prop,dcell_1) ! improv-2

!     This subroutine sums the fluxes into each cell, calculates the change in 
!     the cell property inside, distributes the change to the four nodes of the
!     cell and then adds it onto the flow property

!     Explicitly declare the required variables
      use types
      implicit none
      type(t_appvars), intent(in) :: av
      real, intent(in) :: flux_i(:,:), flux_j(:,:), area(:,:)
      real, intent(inout) :: prop(:,:)
      real, intent(inout) :: dcell_1(:,:) ! improv-2
      real, dimension(size(prop,1),size(prop,2)) :: dnode
      integer :: ni, nj
      ! improv-2
      real :: facsec = 0.5 ! recover: set to 0
      real, dimension(size(dcell_1,1),size(dcell_1,2)) :: dcell_temp, dcell
      dcell_temp = dcell_1

!     Get the block size and store locally for convenience
      ni = size(prop,1); nj = size(prop,2)

!     Use the finite volume method to find the change in the variables "prop"
!     over the timestep "dt", save it in the array "dprop_cell"
!     INSERT
      dcell_1(1:ni-1,1:nj-1) = ( av%dt / area(1:ni-1,1:nj-1) ) * &
                             ( flux_i(1:ni-1,1:nj-1) - flux_i(2:ni,1:nj-1) + &
                               flux_j(1:ni-1,1:nj-1) - flux_j(1:ni-1,2:nj) )

      dcell = (1 + facsec) * dcell_1 - facsec * dcell_temp ! improv-2

!     Now distribute the changes equally to the four corners of each cell. Each 
!     interior grid point receives one quarter of the change from each of the 
!     four cells adjacent to it.
!     INSERT
      dnode(2:ni-1,2:nj-1) = ( dcell(1:ni-2,1:nj-2) + dcell(2:ni-1,1:nj-2) + &
                              dcell(1:ni-2,2:nj-1) + dcell(2:ni-1,2:nj-1) ) / 4

!     Bounding edge nodes do not have four adjacent cells and so must be treated
!     differently, they only recieve half the change from each of the two
!     adjacent cells. Distribute the changes for the "i = 1 & ni" edges as well
!     as the "j = 1 & nj" edges. 
!     INSERT
      dnode(1,2:nj-1) = ( dcell(1,1:nj-2) + dcell(1,2:nj-1) ) / 2
      dnode(ni,2:nj-1) = ( dcell(ni-1,1:nj-2) + dcell(ni-1,2:nj-1) ) / 2
      dnode(2:ni-1,1) = ( dcell(1:ni-2,1) + dcell(2:ni-1,1) ) / 2
      dnode(2:ni-1,nj) = ( dcell(1:ni-2,nj-1) + dcell(2:ni-1,nj-1) ) / 2

!     Finally distribute the changes to be to the four bounding corner points, 
!     these receive the full change from the single cell of which they form one 
!     corner.
!     INSERT
      dnode([1,ni],[1,nj]) = dcell([1,ni-1],[1,nj-1])

!     Update the solution by adding the changes at the nodes "dnode" to the flow
!     property "prop"
!     INSERT
      prop = prop + dnode

      end subroutine sum_fluxes

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine smooth_array(av,prop,corr)

!     This subroutine smooths "prop" to stabilise the calculation, the basic 
!     solver uses second order smoothing, many improvements are possible.

!     Explicitly declare the required variables
      use types
      implicit none
      type(t_appvars), intent(in) :: av
      integer :: ni, nj
      ! improv-3
      real, intent(inout) :: prop(:,:), corr(:,:)
      real, dimension(size(prop,1),size(prop,2)) :: prop_avg, corr_total ! improv-3
      real, dimension(size(prop,1),size(prop,2)) :: prop_avg_2, prop_avg_4, sfac_loc ! improv-4b
      real :: fcorr = 0.9 ! 1.0
      ! improv-4
      real :: sf2, sf4
      sf4 = av%sfac * 4 ! av%sfac = 0.9
      sf2 = sf4 / 4

!     Get the block size and store locally for convenience
      ni = size(prop,1); nj = size(prop,2)

!     Calculate the average values at the nodes in the interior region of the
!     mesh, use the four neighbouring nodes in the plus and minus i and 
!     j-directions.
!     INSERT
      ! prop_avg_2 (2:ni-1,2:nj-1) = ( prop(1:ni-2,2:nj-1) + prop(3:ni,2:nj-1) + &
      !                             prop(2:ni-1,1:nj-2) + prop(2:ni-1,3:nj) ) / 4
      prop_avg_2(2:ni-1,2:nj-1) = ( prop(1:ni-2,1:nj-2) + prop(3:ni,1:nj-2) + &
                                  prop(1:ni-2,3:nj) + prop(3:ni,3:nj) ) / 4 ! jvt24

!     Edge values are also averaged in both the i and j-directions. Parallel to
!     the boundary the averaging is centred, the averages of two nodes are taken
!     either side of the current point. Perpendicular to the boundary the
!     algorithm is one-sided, the value at the current point is extrapolated
!     from the values at two nodes away from the boundary point.
!     INSERT
      prop_avg_2(2:ni-1,1) = ( prop(1:ni-2,1) + prop(3:ni,1) + &
                        2 * prop(2:ni-1,2) - prop(2:ni-1,3) ) / 3
      prop_avg_2(2:ni-1,nj) = ( prop(1:ni-2,nj) + prop(3:ni,nj) + &
                        2 * prop(2:ni-1,nj-1) - prop(2:ni-1,nj-2) ) / 3
      prop_avg_2(1,2:nj-1) = ( prop(1,1:nj-2) + prop(1,3:nj) + &
                        2 * prop(2,2:nj-1) - prop(3,2:nj-1) ) / 3
      prop_avg_2(ni,2:nj-1) = ( prop(ni,1:nj-2) + prop(ni,3:nj) + &
                        2 * prop(ni-1,2:nj-1) - prop(ni-2,2:nj-1) ) / 3

!     The corner values are not currently smoothed
      prop_avg_2([1,ni],[1,nj]) = prop([1,ni],[1,nj])

      ! improv-4a
      prop_avg_4 = prop_avg_2 ! prop_avg_4 = prop
      prop_avg_4(3:ni-2,3:nj-2) = ( prop(2:ni-3,3:nj-2) * 4 - prop(1:ni-4,3:nj-2) + &
                                  prop(4:ni-1,3:nj-2) * 4 - prop(5:ni,3:nj-2) ) / 12 + &
                                  ( prop(3:ni-2,2:nj-3) * 4 - prop(3:ni-2,1:nj-4) + &
                                  prop(3:ni-2,4:nj-1) * 4 - prop(3:ni-2,5:nj) ) / 12

!     Now apply the artificial viscosity by smoothing "prop" towards "prop_avg",
!     take (1-sfac) * the calculated value of the property + sfac * the average 
!     of the surrounding values. 
!     INSERT
      ! improv-4b
      ! fac = min(1.0, abs(prop - prop_avg_2) / maxval(abs(prop - prop_avg_2))) ! fac = 1.0
      ! prop = ( 1 - ( sf2 + sf4 ) * fac ) * prop + ( sf2 * prop_avg_2 + sf4 * prop_avg_4 ) * fac
      sfac_loc = ( sf2 + sf4 ) * abs(prop - prop_avg_2) / maxval(abs(prop - prop_avg_2))
      prop_avg = ( sf2 * prop_avg_2 + sf4 * prop_avg_4 ) / ( sf2 + sf4 )
      ! improv-3
      ! prop_avg = prop_avg_2; sfac_loc = av%sfac ! recover
      corr_total = fcorr * ( prop - prop_avg )
      corr = 0.99 * corr + 0.01 * corr_total
      prop = ( 1 - sfac_loc ) * prop + sfac_loc * ( prop_avg + corr )
      ! prop = ( 1 - sf2 ) * prop + sf2 * ( prop_avg_2 ) ! recover

      end subroutine smooth_array

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      end module stencils


