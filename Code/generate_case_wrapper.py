import argparse

from routines import *
from generate_case import *

del gen_waves
def gen_waves(casename):
    av = default_settings(casename)
    av['p'] = av['pstag'] / 5.751;
    geom = {}
    phi = np.deg2rad(17.13); dx_c = 0.1254; dy_c = 0.01759; 
    dx_bc = 0.0243; dy_bc = 0.08241; dy = 0.1; dx = 0.3; d = 0.0716;
    geom['x_b'] = np.array([-dx, dx_c + dx_bc, dx + dx_c + dx_bc]); 
    geom['y_b'] = np.array([dy, dy, dy + dx * np.tan(phi)]);
    geom['x_a'] = np.array([-dx,0,dx_c,geom['x_b'][-1] + d * np.sin(phi)])
    geom['y_a'] = np.array([0,0,dy_c,geom['y_b'][-1] - d * np.cos(phi)])

    c4 = np.polyfit([-0.05,0,dx_c,0.2],[0,0,dy_c,np.interp(0.2,geom['x_a'],geom['y_a'])],3)
    x_c4 = np.arange(0.01,dx_c-0.01,0.01)
    y_c4 = np.polyval(c4,x_c4)
    geom['x_a'] = np.insert(geom['x_a'], 2, x_c4)
    geom['y_a'] = np.insert(geom['y_a'], 2, y_c4)
    return(av,geom)

p = argparse.ArgumentParser(description='Python wrapper for generate_case.py')
p.add_argument('casename', type=str, help='Name of the case to generate.')
p.add_argument('--cfl', '-c', type=str, help='CFL')
p.add_argument('--sfac', '-s', type=str, help='Smoothing factor')
p.add_argument('--d_max', '-d', type=str, help='Convergence limit')
p.add_argument('--nsteps', '-n', type=str, help='Number of steps')
p.add_argument('--ni', '-i', type=str, help='Number of cells in i-direction')
p.add_argument('--nj', '-j', type=str, help='Number of cells in j-direction')
p.add_argument('--plot', '-p', type=bool, help='Plot the geometry and mesh')
args = p.parse_args()

# Get the casename from command line input
casename = args.casename

# Create the curves for the desired case and set the boundary conditions
if casename == 'bend':
    av,geom = gen_bend(casename)
elif casename == 'bump':
    av,geom = gen_bump(casename)
elif casename == 'tunnel':
    av,geom = gen_tunnel(casename)
elif casename == 'waves':
    av,geom = gen_waves(casename)
elif casename == 'tube':
    av,geom = gen_tube(casename)
elif casename == 'naca':
    av,geom,g = gen_naca(casename)
elif casename == 'turbine_c':
    av,geom,g = gen_turbine_c(casename)
elif casename == 'turbine_h':
    av,geom,g = gen_turbine_h(casename)
elif casename == 'multi':
    av,geom = gen_multi(casename)

if args.cfl:
    av['cfl'] = float(args.cfl)
if args.sfac:
    av['sfac'] = float(args.sfac)
if args.d_max:
    av['d_max'] = float(args.d_max)
if args.nsteps:
    av['nsteps'] = int(args.nsteps)
if args.ni:
    av['ni'] = int(args.ni)
if args.nj:
    av['nj'] = int(args.nj)

# Save the settings and curves to their input files
write_settings(av)
write_geom(av,geom)

if args.plot and args.plot == True: # # plt.ioff()

    # Open figure window to plot the curves
    plt.figure(figsize=[9.6,7.2]); ax = plt.axes(); cols = gen_cols();
    ax.set_xlabel('x / m'); ax.set_ylabel('y / m');
    ax.set_aspect('equal',adjustable='box'); ax.tick_params(direction='in')
    ax.grid(linestyle='-',color=[0.6,0.6,0.6],linewidth=0.5)

    # Plot the geometry curves
    ax.plot(geom['x_a'],geom['y_a'],'.-',color=cols[0,:])
    ax.plot(geom['x_b'],geom['y_b'],'.-',color=cols[1,:])

    # Plot the domain curves if present
    if 'x_c' in geom:
        ax.plot(geom['x_c'],geom['y_c'],'.-',color=cols[2,:])
        ax.plot(geom['x_d'],geom['y_d'],'.-',color=cols[3,:])
    
    # Plot the mesh coordinates and patches if present
    if 'g' in locals():
        
        # Open a new figure window
        plt.figure(figsize=[9.6,7.2]); ax = plt.axes(); cols = gen_cols();
        ax.set_xlabel('x / m'); ax.set_ylabel('y / m');
        ax.set_aspect('equal',adjustable='box'); ax.tick_params(direction='in')

        # Plot the mesh coordinates and walls
        for n in range(g['nn']):

            # Mesh in both i and j-directions
            ax.plot(g['x'][n],g['y'][n],color=cols[n,:],linewidth=0.5)
            ax.plot(np.transpose(g['x'][n]),np.transpose(g['y'][n]),
                color=cols[n,:],linewidth=0.5)

            # Extract the block and plot the walls
            plot_wall(ax,cut_block(g,n),False);

        # Plot the patch matching data
        for m in range(g['nm']):

            # Pull out the index lists and convert to Python indexing
            p = g['match'][m]; n_1 = p['n_1']-1; n_2 = p['n_2']-1;
            i_1 = p['i_1']-1; i_2 = p['i_2']-1; 
            j_1 = p['j_1']-1; j_2 = p['j_2']-1;

            # Extract coordinates
            x_1 = g['x'][n_1][i_1,j_1]; y_1 = g['y'][n_1][i_1,j_1];
            x_2 = g['x'][n_2][i_2,j_2]; y_2 = g['y'][n_2][i_2,j_2];

            # Plot both sides of the patch with different symbols
            ax.plot(x_1,y_1,'+',color=cols[m,:])
            ax.plot(x_2,y_2,'x',color=cols[m,:])

            # Calculate and print the error
            d_max = np.max(np.hypot(x_2 - x_1,y_2 - y_1))
            print('Patch', m+1, 'd_max =', d_max)
            
        # Plot the inlet and outlets, these are assumed to be at i = 1 and i = ni
        x_in = np.squeeze(g['x'][g['n_in']-1][0,:])
        y_in = np.squeeze(g['y'][g['n_in']-1][0,:])
        ax.plot(x_in,y_in,color=[0.8,0.8,0.8])
        x_out = np.squeeze(g['x'][g['n_out']-1][-1,:])
        y_out = np.squeeze(g['y'][g['n_out']-1][-1,:])
        ax.plot(x_out,y_out,color=[0.8,0.8,0.8])

        # Write the entire grid definition to file
        write_mesh(av,g)

    # Show all the plots
    plt.show()
