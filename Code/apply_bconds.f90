      
      subroutine apply_bconds(av,g,bcs,ss,ch) ! ext-2

!     This subroutine applies both the inlet and outlet boundary conditions, as
!     it modifies both the primary and secondary flow variables they must be
!     calculated first

!     Explicitly declare the required variables
      use types
      implicit none
      type(t_appvars), intent(in) :: av
      type(t_grid), intent(inout) :: g
      type(t_bconds), intent(inout) :: bcs
      logical, intent(in) :: ss ! ext-2
      logical, intent(in) :: ch ! improv-8

!     Declare the other variables you need here
!     INSERT
      real :: t(1:av%ni,1:av%nj), v(1:av%ni,1:av%nj)

!     At the inlet boundary the change in density is driven towards "rostag",
!     which is then used to obtain the other flow properties to match the
!     specified stagnation pressure, temperature and flow angle. 

!     To help prevent instabilities forming at the inlet boundary condition the 
!     changes in inlet density are relaxed by a factor "rfin" normally set to 
!     0.25 but it can be reduced further.

!     It is also worth checking if "ro" is greater than "rostag" and limiting 
!     the values to be slightly less than "rostag". This can prevent the solver 
!     crashing during severe transients.
      ! ext-2
      if(ss) then
            if(av%casename == 'waves') then
                  g%ro(1,:) = bcs%rostag * ( 1/5.751 ) ** (1 / av%gam)
            else
                  bcs%ro = bcs%rostag * (bcs%p_out / bcs%pstag) ** (1 / av%gam)
            end if
      end if
      if(av%nstep == 1) then
          bcs%ro = g%ro(1,:)
      else
          bcs%ro = bcs%rfin * g%ro(1,:) + (1 - bcs%rfin) * bcs%ro
      endif
      bcs%ro = min(bcs%ro,0.9999 * bcs%rostag)

!     Calculate "p(1,:)", "rovx(1,:)", "rovy(1,:)" and "roe(1,:)" from the inlet 
!     "ro(:)", "pstag", "tstag" and "alpha". Also set "vx(1,:)", "vy(1,:)" and 
!     "hstag(1,:)"
!     INSERT
      if(av%casename /= 'tube') then ! ext-2
      t(1,:) = bcs%tstag * (bcs%ro / bcs%rostag) ** (av%gam - 1)
      v(1,:) = sqrt(2 * av%cp * (bcs%tstag - t(1,:)))
      g%p(1,:) = bcs%pstag * (bcs%ro / bcs%rostag ) ** av%gam
      ! g%p(1,:) = bcs%ro * av%rgas * t(1,:)
      g%vx(1,:) = v(1,:) * cos(bcs%alpha)
      g%vy(1,:) = v(1,:) * sin(bcs%alpha)
      g%rovx_start(1,:) = bcs%ro * g%vx(1,:) ! improv-1
      g%rovy_start(1,:) = bcs%ro * g%vy(1,:) ! improv-1
      ! improv-8
      if(.not. ch) then
            ! g%roe(1,:) = g%p(1,:) / (av%gam - 1) + 0.5 * bcs%ro * (g%vx(1,:) ** 2 + g%vy(1,:) ** 2)
            g%roe_start(1,:) = bcs%ro * (av%cv * t(1,:) + 0.5 * v(1,:) ** 2) ! improv-1
            g%hstag(1,:) = ( g%roe(1,:) + g%p(1,:) ) / bcs%ro
      end if
      end if

!     For the outlet boundary condition set the value of "p(ni,:)" to the
!     specified value of static pressure "p_out" in "bcs"
!     INSERT
      ! ext-2
      if(.not. ss) then
            g%p(av%ni,:) = bcs%p_out
      end if

      end subroutine apply_bconds


