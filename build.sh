#!/bin/bash

ARGS="$@"
while (( "$#" )); do
  case "$1" in
    bend|bump|tunnel|waves|tube|naca|turbine_c|turbine_h)
        CASE=$1; echo -e "\033[0;32m- Case: $CASE\033[0m"; shift
        ;;
    -c|--cfl)
        if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then cfl=$2; shift 2; fi
        ;;
    -s|--sfac)
        if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then sfac=$2; shift 2; fi
        ;;
    -d|--d_max)
        if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then d_max=$2; shift 2; fi
        ;;
    -n|--nsteps)
        if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then nsteps=$2; shift 2; fi
        ;;
    -i|--ni)
        if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then ni=$2; shift 2; fi
        ;;
    -j|--nj)
        if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then nj=$2; shift 2; fi
        ;;
    -a|--auto)
        auto=1; shift
        ;;
    *)
        echo "Usage: $0 [bend|bump|tunnel|waves|tube|naca|turbine_c|turbine_h] [-c CFL] [-s SFAC] [-d D_MAX] [-n NSTEPS] [-i NI] [-j NJ] [-p [0|1]]"; exit
        ;;
  esac
done

if [ -z "${cfl}" ]; then cfl=0.4; fi
if [ -z "${sfac}" ]; then sfac=0.5; fi
if [ -z "${d_max}" ]; then d_max=0.0001; fi
if [ -z "${nsteps}" ]; then nsteps=5000; fi
if [ -z "${ni}" ]; then ni=53; fi
if [ -z "${nj}" ]; then nj=37; fi

if [ ! -d Cases ]; then mkdir -p Cases; fi; cd Cases

echo -e "\033[0;32m- Compiling\033[0m"
if [ $(uname -s) == "Darwin" ]; then
    echo '- OS: macOS'; source ~/Anaconda/rc
    sed() { gsed "$@"; }; export -f sed
else echo '- OS: Linux'; fi
cd ../Code/; make clean; make; cd ../Cases/

run() {
    echo -e "\033[0;32m- Running\033[0m"; 
    echo -e "- CFL: $cfl\n- SFAC: $sfac\n- D_MAX: $d_max\n- NSTEPS: $nsteps\n- NI: $ni\n- NJ: $nj"
    # timeout 5s python3 ../Code/generate_case_wrapper.py ${ARGS#* } $CASE;
    timeout 5s python3 ../Code/generate_case_wrapper.py -c $cfl -s $sfac -d $d_max -n $nsteps -i $ni -j $nj $CASE; sleep 2
    utime="$(TIMEFORMAT='%U';time ( ../Code/solver.x < input_$CASE.txt ) 2>&1 1>/dev/null )"
    if [ ! -d $CASE ]; then mkdir -p $CASE; fi
    if [[ "$utime" == *"exceptions"* ]]; then
        echo -e "\033[0;31m- Diverged\033[0m"
        touch out_final_${CASE}_${cfl}_${sfac}_${d_max}_${nsteps}_${ni}_${nj}_div.bin
    else
        echo -e "\033[0;32m- Plotting\033[0m";
        # timeout 5s python3 ../Code/plot_coord.py $CASE &
        timeout 15s python3 ../Code/plot_contours.py $CASE &
        timeout 15s python3 ../Code/plot_conv.py $CASE &
        timeout 2s python3 ../Code/plot_guess.py $CASE; sleep 2
        mv out_final_$CASE.bin out_final_${CASE}_${cfl}_${sfac}_${d_max}_${nsteps}_${ni}_${nj}_${utime}.bin
        mv out_coord_$CASE.bin out_coord_${CASE}_${cfl}_${sfac}_${d_max}_${nsteps}_${ni}_${nj}_${utime}.bin
        mv out_guess_$CASE.bin out_guess_${CASE}_${cfl}_${sfac}_${d_max}_${nsteps}_${ni}_${nj}_${utime}.bin
        mv conv_$CASE.csv conv_${CASE}_${cfl}_${sfac}_${d_max}_${nsteps}_${ni}_${nj}_${utime}.csv
    fi
    mv *_${CASE}_* $CASE/
    mv input_$CASE.txt $CASE/
    echo -e "\033[0;32m- Saved\033[0m";
}


if [ "$auto" == 1 ]; then
    if [ "$CASE" == "bend" ] || [ "$CASE" == "bump" ] ; then
        d_max=0.0001; nsteps=8000
    elif [ "$CASE" == "waves" ]; then
        d_max=0.01; nsteps=1800
    fi
    for cfl in 0.2 0.4 0.6; do
        for sfac in 0.2 0.4 0.8; do
            for coords in "212 148"; do # "212 148"
                read ni nj <<< $coords
                run
            done
        done
    done
else
    run
fi
