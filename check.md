# Completed
`euler_iteration.f90`
`stencils.f90`:`sum_fluxes`
`apply_bconds.f90`
`set_timestep.f90`
`calc_areas.f90`
`set_secondary.f90`
`flow_guess.f90`

# Issue Tracker
`stencils.f90`:`smooth_array` verify smoothing for inlet and outlet
`routines.py`:`calc_secondary` sensitive to cumulative errors
